\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[backend=biber]{biblatex}
\usepackage{enumerate}
\usepackage[margin=5em]{geometry}

\addbibresource{../shared/bibliography.bib}

\setlength\parindent{0em}
\setlength\parskip{1em}

\title{%
	\textbf{Master Thesis -- Research Plan:}\\
	On the Influence of Tokenizers in NLP Pipelines
}

\author{%
	{\small author:}\\
	Hendrik Werner\\
	\textit{s4549775}
	\and
	{\small supervisor:}\\
	Arjen P. De Vries
}

\begin{document}

\maketitle

\begin{table}
	\centering
	\begin{tabular}{rl}
		\textbf{start date:} & 2021-10-18\\
		\textbf{expected end date:} & 2022-05-01\\
		\textbf{study program:} & Computing Science\\
	\end{tabular}
\end{table}

\clearpage
\section{Introduction}

Most contemporary state-of-the-art Natural Language Processing (NLP) models operate on the token level, and involve the use of tokenizers during the encoding and decoding process~\cite{ALBERT:lan2020}~\cite{BERT:devlin2019}~\cite{DeBERTa:he2021}~\cite{ELECTRA:clark2020}~\cite{GPT-3:brown2020}. Tokenizers are responsible for translating between text and a vocabulary of tokens the models can operate on. In the past, they have typically been treated as independent entities, and were created separately from the models themselves. Existing tokenization approaches are complex, language specific, and susceptible to noise.

It is also possible to operate on raw text, by replacing tokens with byte sequences, resulting in token-free models. Recent research~\cite{ByT5:xue2021} has shown that removing tokenizers from the process, by operating directly on the byte level, comes with many advantages, including a simplified architecture and improved genericity and robustness. Existing models can be applied to raw text with minimal modification.

Moreover, complex natural language processing pipelines can involve the use of different tokenizers at different stages. For example, BERT could be used to extract named entities from a text, which are then used to rank documents using BM25. Both BERT and BM25 come with their own, independent tokenizers, which are not only separate from the models themselves, but also not optimized to be used in tandem.

\section{Research Questions}

During my master thesis, I want to investigate the influence of tokenizers in a realistic context, at different stages of NLP pipelines; and how the choices of tokenizers at those stages interact. Existing research is focused on measuring the effects of changing or removing tokenizers in isolation. In most cases, this is not a good representation of real-world use cases. NLP pipelines consist of multiple constituents, which often entail several different tokenizers.

Specifically, I propose the following research questions:

\begin{itemize}
	\item Can the effects of different tokenizers and token-free models, which have been demonstrated in isolation, be reproduced in a realistic NLP pipeline?
	\item Is it possible to remove tokenizers from more stages of NLP pipelines, possibly entirely?
	\item How do tokenizer choices at different stages of NLP pipelines interact?
		\begin{itemize}
			\item Does the use of different tokenizers at different stages affect performance? It is better to use a single tokenizer throughout?
			\item Is there unexplored optimization potential, by accounting for interactions between multiple tokenizers?
		\end{itemize}
\end{itemize}

\section{Approach}

To answer the posed research questions, the plan is to build representative NLP pipelines with the Anserini~\cite{Anserini:yang2017} toolkit for reproducible information retrieval research. Some metrics will have to be set in order to measure the performance of the overall pipeline. Once a quantification framework has been put into place and a baseline established, tokenizers at different stages can be switched out or removed, and the effects measured.

\printbibliography

\end{document}
