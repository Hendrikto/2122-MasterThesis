# Pinyin celebrates 50th birthday

* by Xinhua News Agency, 2008-02-11

Hanyu Pinyin, or the Chinese phonetic system, will celebrate its 50th anniversary on Monday (Feb. 11). One billion Chinese have used it to learn mandarin since the first edition of pinyin was issued in 1958.

"Pinyin is useful. It helps us to learn Chinese characters. Thanks to pinyin, we learnt how to read," 92-year-old Chen Douxiang from Wanrong County, northern Shanxi Province, still remembered the pinyin poem she learned 50 years ago.

The first edition of Pinyin was adopted at the Fifth Session of the First National People's Congress on Feb. 11, 1958. It was then introduced to primary schools, and used to improve the literacy rate among adults. By the end of 1959, two-thirds of residents in Wanrong County had learnt Mandarin using Pinyin.

"About one billion Chinese citizens have mastered pinyin, which plays an important role in both Chinese language education and international communication," said Wang Dengfeng, vice-chairman of the National Language Committee and director of language department of the Education Ministry.

The Chinese Braille, based on pinyin, has helped blind people to learn Chinese language, knowledge and skills.

The Chinese government also helped 12 minority groups including Zhuang, Buyi, and Hani to establish Latin forms of their languages, based on the pinyin system.

The government used all kinds of measures to popularize pinyin for schools, newspapers, and brand names, and standardize the usage of pinyin on radios, TV and in advertisements.

The five mascots for Beijing 2008 Olympics, which were initially called "Friendlies", have been renamed "Fuwa", a literal Chinese pinyin translation. Many Chinese take the new name as more acceptable and easy to understand.

China also published the Xinhua Dictionary and Modern Chinese Dictionary, setting rules for using pinyin to spell people's names and places. In 2001, the government issued the National Common Language Law providing a legal basis for applying pinyin.

Zhou Youguang, a 102-year old linguist has worked on the research and application of pinyin for almost half a century. He said the speed of the popularization of pinyin was amazing and pinyin was used not only in education but also industry and commerce. It used to be regarded only as a tool for learning characters in primary schools, but now it serves as a communication tool. It used to be the key to Chinese culture, but now it is a bridge linking China to the whole world.

"As a cultural tool, Pinyin is easy to learn and use and meets the needs of times, so it can spread all by itself," Zhou added.

The popularization of pinyin is a demonstration of the development of China. Now more international students are learning Mandarin and Chinese culture.

"Hanyu Pinyin not only belongs to China but also belongs to the world. It is now everywhere in our daily lives," Wang said, "Pinyin will continue playing an important role in the modernization of China."
