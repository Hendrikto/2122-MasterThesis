# ImageNet: A Large-Scale Hierarchical Image Database

## Summary

* Created an ontology for images, based on WordNet.
* Demonstrated how ImageNet is better than existing datasets.

## Main Contributions

* introduced ImageNet
  * made an accurate, diverse, hierarchical, large-scale, labeled database of images widely available
* demonstrated usefulness of ImageNet
  * for both training and evaluation

## Quotes

* There has been an explosion of available data.

  > The digital era has brought with it an enormous explosion of data.

* Better models can be trained with more data:

  > More sophisticated and robust models and algorithms can be proposed by exploiting these images, resulting in better applications for users to index, retrieve, organize and interact with these data.

  > As computer vision research advances, larger and more challenging datasets are needed for the next generation of algorithms.

* Stated goals:

  > Scale: ImageNet aims to provide the most comprehensive and diverse coverage of the image world.
  > Hierarchy: ImageNet organizes the different classes of images in a densely populated semantic hierarchy.
  > Accuracy: We would like to offer a clean dataset at all levels of the WordNet hierarchy.
  > Diversity: ImageNet is constructed with the goal that objects in images should have variable appearances, positions, view points, poses as well as background clutter and occlusions.

  > Our goal is to complete the construction of around 50 million images in the next two years.
  > At the completion of ImageNet, we aim to […] have roughly 50 million clean, diverse and full resolution images spread over approximately 50K synsets;

## Notes

* The goal was to have 50 million images across 50 thousand synsets indexed by 2011. As of June 2022, merely ~14M images (28%), and ~22k synsets (44%) were indexed.
