# A Neural Probabilistic Language Model

## Summary

* Developed a NN-based language model, based on word embeddings.
* Showed how such a model can be trained.
* Obtained SotA performance with that model.
  * 10-20% improvement

## Main Contributions

* introduced word embeddings (called "word feature vectors") for language modelling
  * only on the input side
* showed that word embeddings generalize better than previous methods
* demonstrated that end-to-end learning is great
  * by training word embeddings and LM at the same time

## Quotes

* Continuous spaces are better suited towards model training:

  > When modeling continuous variables, we obtain generalization more easily […] because the function to be learned can be expected to have some local smoothness properties. For discrete spaces, the generalization structure is not as obvious: any change of these discrete variables may have a drastic impact on the value of the function to be estimated, and when the number of values that each discrete variable can take is large, most observed objects are almost maximally far from each other in Hamming distance.

  > […] "similar" words are expected to have a similar feature vector, and because the probability function is a smooth function of these feature values, a small change in the features will induce a small change in the probability. Therefore, the presence of only one of the above sentences in the training data will increase the probability, not only of that sentence, but also of its combinatorial number of "neighbors" in sentence space (as represented by sequences of feature vectors).

* Word embeddings were only used on the input side:

  > In the neural network described […] the distributed word features are used only for the "input" words and not for the "output" word (next word). Furthermore, a very large number of parameters (the majority) are expanded in the output layer: the semantic or syntactic similarities between output words are not exploited.

* Contextual word embeddings can handle OOV tokens:

  > [This architecture] can easily deal with out-of-vocabulary words (and even assign them a probability!). The main idea is to first guess an initial feature vector for such a word, by taking a weighted convex combination of the feature vectors of other words that could have occurred in the same context, with weights proportional to their conditional probability. […] We can then incorporate [the new word into the vocabulary] and re-compute probabilities for this slightly larger set […]. This feature vector […] can then be used in the input context part when we try to predict the probabilities of words that follow [that] word […].

* Polysemy is not dealt with properly by single, static embeddings:

  > Polysemous words are probably not well served by the model presented here, which assigns to each word a single point in a continuous semantic space. We are investigating extensions of this model in which each word is associated with multiple points in that space, each associated with the different senses of the word.

## Literature Review

* Using NNs for language modelling:
  * Risto Miikkulainen and Michael G. Dyer. "Natural language processing with modular neural networks and distributed lexicon". Cognitive Science, 1991.
  * Wei Xu and Alexander Rudnicky. "Can artificial neural networks learn language models?". International Conference on Statistical Language Processing, 2000.
* Discovering and exploiting similarity between words:
  * Peter F. Brown and Vincent J. Della Pietra and Peter V. deSouza and Jenifer C. Lai and Robert L. Mercer. "Class-Based n-gram Models of Natural Language". Association for Computational Linguistics, 1992.
  * Fernando Pereira and Naftali Tishby and Lillian Lee. "Distributional Clustering of English Words". Association for Computational Linguistics, 1993.
  * Douglas Baker and Andrew McCallum. "Distributional clustering of words for text classification". SIGIR, 1998.
  * T. R. Niesler and E. W. D. Whittaker and P. C. Woodl. "Comparison of Part-of-speech and Automatically Derived Category-based Language Models for Speech Recognition". International Conference on Acoustics, Speech and Signal Processing, 1998.
* Representing words in vector space:
  * Hinrich Schütze. "Word Space". Advances in Neural Information Processing Systems 5, 1993.
* Feeding NNs with vector space representations:
  * Kare Jean Jensen and Søren Riis. "Self-organizing letter code-book for text-to-phoneme neural network model". International Conference on Speech and Language Processing, 2000.
