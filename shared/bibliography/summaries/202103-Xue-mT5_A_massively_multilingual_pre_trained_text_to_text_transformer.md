# mT5: A Massively Multilingual Pre-trained Text-to-Text Transformer

## Summary

* Introduced mT5, an adapted, multilingual version of the T5 architecture.
* Compiled multilingual pre-training dataset mC4, covering 101 languages (with 107 variants).
* Obtained SotA results on several benchmarks, by training mT5 on mC4 with a new training method.
  * Since all fine-tuning data is English, the model can learn to partially translate other languages.
  * This can be counteracted through DPT, by mixing in unsupervised pre-training examples during fine-tuning.

## Main Contributions

* made T5 architecture multilingual
* introduced multilingual version of C4 dataset, called mC4
* presented new training method to counteract "accidental translation"
  * called Domain Preserving Training (DPT)

## Quotes

* Transfer learning is prevalent in contemporary NLP pipelines:

  > Current natural language processing (NLP) pipelines often make use of transfer learning.

* Transfer learning allows amortizing the most expensive work:

  > […] checkpoints allow members of the NLP community to quickly attain strong performance on many tasks without needing to perform expensive pre-training themselves.

* SotA performance can be achieved by building on foundation models:

  > […] pre-trained checkpoints for the “Text-to-Text Transfer Transformer” (T5) model […] have been used to achieve state-of-the-art results on many benchmarks.

* Lots of data is needed for pre-training:

  > […] a model is pre-trained on a data-rich task before being fine-tuned on a downstream task of interest.

* It is more difficult to compile non-English training data:

  > Since some of these languages are relatively scarce on the internet, we make use of all of the 71 monthly web scrapes released so far by Common Crawl. This is dramatically more source data than was used for C4, for which the April 2019 web scrape alone was enough to provide plenty of English-language data.

* mT5 inherits benefits of T5, by closely following its architecture:

  > Our goal with mT5 is to produce a […] model that deviates as little as possible from the recipe used to create T5. As such, mT5 inherits all of the benefits of T5 […], such as its general-purpose text-to-text format, its design based on insights from a large-scale empirical study, and its scale.

* Text-to-text framework is more natural for generative tasks, compared to classification:

  > [A text-to-text] approach is natural for generative tasks (such as machine translation or abstractive summarization) where the task format requires the model to generate text conditioned on some input. It is more unusual for classification tasks, where T5 is trained to output the literal text of the label (e.g. "positive" or "negative" for sentiment analysis) instead of a class index.

* Main advantage of unified text-to-text architecture:

  > The primary advantage of [the unified text-to-text] approach is that it allows the use of exactly the same training objective (teacher-forced maximum-likelihood) for every task, which in practice means that a single set of hyperparameters can be used for effective fine-tuning on any downstream task.

* In general, models scale extremely well with their number of parameters:

  > Overall, our results highlight the importance of model capacity in cross-lingual representation learning and suggest that scaling up a simple pre-training recipe can be a viable alternative to more complex techniques relying on LM filtering, parallel data, or intermediate tasks.

  > […] model capacity is key to improving performance […]

* Higher-capacity models are easier to train:

  > For the smallest model, training on gold datasets (in-language multitask) achieves dramatically better performance than using weakly supervised data (translate-train) or English-only data (zero-shot), whereas the gap between these three settings is much smaller for the largest model.

  > For our two largest models, zero-shot and translate-train performance is nearly the same, showing that machine translations of the monolingual dataset bring diminishing returns as model capacity increases. Overall, these trends point to the possibility of avoiding the costly step of annotating data in more than one language when using large models.

* With large enough models, multilingual models perform well in single-language settings:

  > While the Small and Base mT5 models fall short of their English T5 counterparts [on single-language tasks], we find that the larger models close the gap. This suggests there may be a turning point past which the model has enough capacity to effectively learn 101 languages without significant interference effects.

* Generative models do not necessarily produce "legal" output:

  > Since mT5 is a generative model, it can output arbitrary text predictions in a free form fashion.

  > In using a generative model for span selection […], we hope the model learns to generate “legal” spans that are substrings of the provided context. However, unlike encoder-based models like BERT, this is not a hard constraint of the model.

## Literature Review

* Non-english, single-language versions of Transformer language models:
  * BERT:
    * Marco Polignano, Pierpaolo Basile, Marco de Gemmis, Giovanni Semeraro, and Valerio Basile. "AlBERTo: Italian BERT Language Understanding Model for NLP Challenging Tasks Based on Tweets". CLiC-it, 2019.
    * Wietse de Vries, Andreas van Cranenburgh, Arianna Bisazza, Tommaso Caselli, Gertjan van Noord, and Malvina Nissim. "BERTje: A Dutch BERT Model". arXiv:1912.09582, 2019.
    * Dat Quoc Nguyen and Anh Tuan Nguyen. "PhoBERT: Pre-trained Language Models for Vietnamese". Findings of the Association for Computational Linguistics, 2020.
    * Hang Le, Loïc Vial, Jibril Frej, Vincent Segonne, Maximin Coavoux, Benjamin Lecouteux, Alexandre Allauzen, Benoit Crabbé, Laurent Besacier, and Didier Schwab. "FlauBERT: Unsupervised Language Model Pre-training for French". Proceedings of the 12th Language Resources and Evaluation Conference, 2020.
    * Louis Martin, Benjamin Muller, Pedro Javier Ortiz Suárez, Yoann Dupont, Laurent Romary, Éric de la Clergerie, Djamé Seddah, and Benoît Sagot. "CamemBERT: a Tasty French Language Model". Proceedings of the 58th Annual Meeting of the Association for Computational Linguistics, 2020.
    * Martin Malmsten, Love Börjeson, and Chris Haffenden. "Playing with Words at the National Library of Sweden -- Making a Swedish BERT". arXiv:2007.01658, 2020.
    * Pieter Delobelle, Thomas Winters, and Bettina Berendt. "RobBERT: a Dutch RoBERTa-based Language Model". arXiv:2001.06286, 2020.
  * T5:
    * Diedre Carmo, Marcos Piau, Israel Campiotti, Rodrigo Nogueira, and Roberto Lotufo. "PTT5: Pretraining and validating the T5 model on Brazilian Portuguese data". arXiv:2008.09144, 2020.
* Multilingual versions of Transformer language models:
  * BERT:
    * Jacob Devlin. https://github.com/google-research/bert/blob/master/multilingual.md. GitHub, 2018.
  * BART:
    * Yinhan Liu, Jiatao Gu, Naman Goyal, Xian Li, Sergey Edunov, Marjan Ghazvininejad, Mike Lewis, and Luke Zettlemoyer. "Multilingual Denoising Pre-training for Neural Machine Translation". arXiv:2001.08210, 2020.
  * RoBERTa:
    * Alexis Conneau, Kartikay Khandelwal, Naman Goyal, Vishrav Chaudhary, Guillaume Wenzek, Francisco Guzmán, Edouard Grave, Myle Ott, Luke Zettlemoyer, and Veselin Stoyanov. "Unsupervised Cross-lingual Representation Learning at Scale". Proceedings of the 58th Annual Meeting of the Association for Computational Linguistics, 2020.

## Notes

* Model architecture:
  * based on T5.1.1 recipe
  * only unsupervised pre-training
  * larger vocabulary size of 250k, with byte-fallback for unknown tokens
  * also increased parameter count over T5, to compensate for larger vocabulary

* Training parameters:
  * pre-training:
    * dataset: mC4
    * 1 million steps with batch size = 1024 on input sequences of length 1024
      * roughly 1 trillion tokens in total
      * same as T5, and only 1/6 of XLM-R
    * learning rate $\eta = 1 / \sqrt{\max (n, k)}$
      * $n$ is the iteration
      * $k = 10^4$ is the number of warmup steps
    * dropout rate = 0
    * same objective as T5
      * 15% of tokens masked
      * average span length = 3
    * proven technique for boosting probability of sampling from uncommon languages
      * previously used by mBERT, XLM-R and MMNMT
      * empirically, $\alpha = 0.3$ was found to yield best performance
  * fine-tuning:
    * constant learning rate $\eta = 0.001$
    * dropout rate = 0.1
    * batch size $\in \{2^{17}, 2^{20}\}$
    * DPT: pre-training data is mixed in, to prevent accidental translation
      * sentinel tokens are removed
      * parameter $\alpha$ is decreased to 0.1, to sample uniformly across all languages
      * data is mixed into fine-tuning dataset at ratio of 1:100
    * checkpoints taken every 200 steps
    * model with best validation performance is chosen

* Representation of NER task:

  > […] if there are multiple entities, then they are concatenated in the order they appear, and if there are no entities then the target text is “None”.
