# Text Chunking using Transformation-Based Learning

## Summary

* Trained a (at the time) SotA text chunking model with transformation-based learning on BIO-encoding v1.
  * precision of 92%
* Model is still rule-based, even though ML techniques are employed to derive those rules.

## Main Contributions

* invented BIO-encoding v1 (aka IOB)
  * see section 4.1
* applied transformation-based learning to a new domain
  * including some optimizations to make this feasible

## Quotes

* Definition of text-chunking:

  > Text chunking involves dividing sentences into nonoverlapping segments on the basis of fairly superficial analysis.

* Tagging words is simpler than using delimiters:

  > In the text-chunking application, encoding the predicted chunk structure in tags attached to the words, rather than as brackets between words, avoids many of the difficulties with unbalanced bracketings that would result if such local rules were allowed to insert or alter inter-word brackets directly.

  > Encoding chunk structure with tags attached to words rather than non-recursive bracket markers inserted between words has the advantage that it limits the dependence between different elements of the encoded representation. While brackets must be correctly paired in order to derive a chunk structure, it is easy to define a mapping that can produce a valid chunk structure from any sequence of chunk tags; the few hard cases that arise can be handled completely locally.

* Larger training sets are beneficial to model performance:

  > […] training set size has a significant effect on the results […]

  * Table 7
  * Table 8

## Notes

* Transformation-based learning
  * Has nothing to do with Transformer models.
  * Rather, it is a ML training paradigm that iteratively learns an ordered sequence of transformation rules.
  * Was invented by Brill and Eric in 1993, in the Ph.D. thesis "A Corpus-Based Approach to Language Learning".

* Hard metrics were used.
  * i.e. chunks had to match exactly
