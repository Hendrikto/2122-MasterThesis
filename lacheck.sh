#!/usr/bin/env bash

if (($# != 1)); then
	printf 'usage: %s <path>\n' "${0}"
	exit 1
fi

directives=$(grep -noP '% lacheck ignore:\K.+$' "${1}")

pattern=''
if ((${#directives})); then
	while read -r ignore; do
		pattern+="(?:line ${ignore})|"
	done <<< "${directives}"
	pattern=${pattern::-1}
else
	pattern='$ ' # does not match anything
fi

lacheck "${1}" | grep -vP "${pattern}"

exit 0
