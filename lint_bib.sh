#!/usr/bin/env bash

RED='\e[0;31m'
GREEN='\e[0;32m'
BLUE='\e[0;34m'
RESET='\e[m'

n_problems_total=0

for file in "${@}"; do
	# find problems
	problems=$(biber --output-file '/dev/null' --tool --validate-datamodel "${file}" | grep --invert-match '^INFO')
	[[ "${#problems}" = 0 ]] && unset 'problems[0]'
	# count problems
	n_problems="${#problems[@]}"
	# update global counter
	((n_problems_total += n_problems))

	# show problems
	[[ "${n_problems}" > 0 ]] && echo "${problems[@]}"
	# show file problem count
	echo -e "${BLUE}${file}${RESET}: ${n_problems}"
done

# show global problem count
[[ $n_problems_total = 0 ]] && color="${GREEN}" || color="${RED}"
echo -e "${color}Total:${RESET} ${n_problems_total}"

exit ${n_problems_total}
