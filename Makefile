XDG_CACHE_HOME ?= '~/.cache'
XDG_STATE_HOME ?= '~/.local/state'

cache_dir := "$(XDG_CACHE_HOME)/2122-MasterThesis"
state_dir := "$(XDG_STATE_HOME)/2122-MasterThesis"

bib_files := $(shell find . -name '*.bib')
py_files := $(shell find implementation -name '*.py')
tex_files := $(shell find . -name '*.tex')

tracked_py_files != git ls-files -- $(py_files)

ignored_LaTeX_cache_files := $(shell git check-ignore \
	$(wildcard research_plan/*) \
	$(wildcard shared/*) \
	$(wildcard thesis/*) \
)

.PHONY: clean
clean: clean_cache clean_LaTeX

.PHONY: clean_cache
clean_cache:
	############################ Cleaning Project Cache ############################
	# removing cache directory…
	$(RM) -r $(cache_dir)

.PHONY: clean_LaTeX
clean_LaTeX:
	############################# Cleaning LaTeX Files #############################
	# cleaning ignored cache files…
	$(RM) $(ignored_LaTeX_cache_files)

.PHONY: clean_state
clean_state:
	############################ Cleaning Project State ############################
	# removing state directory…
	$(RM) -r $(state_dir)

.PHONY: lint
lint: lint_BibLaTeX lint_LaTeX

.PHONY: lint_BibLaTeX
lint_BibLaTeX: $(bib_files)
	############################ Linting BibLaTeX Files ############################
	# linting with lint_bib.sh…
	@./lint_bib.sh $(bib_files)

.PHONY: lint_LaTeX
lint_LaTeX: $(tex_files)
	########################### Linting LaTeX Documents ############################
	# linting with lacheck…
	@xargs -n1 ./lacheck.sh <<< "$(tex_files)"
	# linting with chktex…
	@chktex --quiet $(tex_files)

.PHONY: lint_Python
lint_Python:
	############################# Linting Python Files #############################
	# linting with flake8…
	@flake8 $(tracked_py_files)
	# linting with isort…
	@isort --check $(tracked_py_files)
